package org.diopm;

import java.util.Date;

import org.diopm.dao.ClientRepository;
import org.diopm.dao.CompteRepository;
import org.diopm.dao.OperationRepository;
import org.diopm.entities.Client;
import org.diopm.entities.Compte;
import org.diopm.entities.CompteCourant;
import org.diopm.entities.CompteEpargne;
import org.diopm.entities.Operation;
import org.diopm.entities.Retrait;
import org.diopm.entities.Versement;
import org.diopm.metier.IBanqueMetier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class BanqueApplication implements CommandLineRunner{

	@Autowired
	private ClientRepository clientRepository;
	@Autowired
	private CompteRepository compteRepository;
	@Autowired
	private OperationRepository operationRepository;
	@Autowired
	private IBanqueMetier banqueMetier;
	
	
	public static void main(String[] args) {
		
	  SpringApplication.run(BanqueApplication.class, args);
	  
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		Client c1= clientRepository.save(new Client("Moussa","moussa@gmail.com"));
		Client c2 =  clientRepository.save(new Client("Diop","diop@gmail.com"));
		
		Compte cp1 = compteRepository.save(new CompteCourant("c1", new Date(), 90000, c1, 60000));
		Compte cp2 = compteRepository.save(new CompteEpargne("c2", new Date(), 60000, c2, 5.5));
		
		operationRepository.save(new Versement(new Date(), 9000, cp1));
		operationRepository.save(new Versement(new Date(), 6000, cp1));
		operationRepository.save(new Versement(new Date(), 2300, cp1));
		operationRepository.save(new Retrait(new Date(), 9000, cp1));
		
		operationRepository.save(new Versement(new Date(), 2300, cp2));
		operationRepository.save(new Versement(new Date(), 400, cp2));
		operationRepository.save(new Versement(new Date(), 2300, cp2));
		operationRepository.save(new Retrait(new Date(), 3000, cp2));
		
		banqueMetier.verser("c1", 11111);
	}

}
